<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Owners_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

   public function callAPI($method, $url, $data){
               
        $curl = curl_init();
        $headers = array(
            'Accept:application/json',
            'Content-Type: application/json',
            'Authorization: Bearer bdcf2f6c9a6ca4950f613d56bb16bbaf6a56bd3bc000fb130ad2c16eb36a99d8',
        );

        curl_setopt_array($curl, array(
          CURLOPT_URL => $url,
          // curl_setopt($curl, CURLOPT_POST, 1);
          CURLOPT_HTTPHEADER => $headers,
          CURLOPT_RETURNTRANSFER => true,
        ));


       // EXECUTE:
       $result = curl_exec($curl);
       if(!$result){die("Connection Failure");}
       curl_close($curl);
       return $result;
    }

    public function get_all_user(){
        $users_encode = self::callAPI('POST', 'https://gorest.co.in/public/v1/users?page=1', false);
        for ($i=1; $i < json_decode($users_encode)->meta->pagination->pages; $i++) { 
            $users_page[$i] = self::callAPI('POST', 'https://gorest.co.in/public/v1/users?page=1', false);
            if ($i == 1){
                $users = array_merge(json_decode($users_encode)->data, json_decode($users_page[$i])->data);
            }else{
                $users = array_merge($users, json_decode($users_page[$i])->data);
            }
        }
        foreach ($users as $key => $value) {
            if ($value->status == 'inactive'){
                unset($users[$key]);
            }
        }
        return $users;
    }
    public function get_user($id){
        $users_encode = self::callAPI('POST', 'https://gorest.co.in/public/v1/users?id=1', false);
        
        return json_decode($users_encode)->data;
    }
}