<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Companies_model extends CI_Model {

    public function __construct()
    {
        parent::__construct();
    }

    //Obtenemos las empresas del usuario $id_user
    public function get_companies_user($page = false)
    {
        if ($page){
            $start = intval($page)*20;
            
           $query = $this->db->order_by('id ASC')
                                ->limit(20,$start)
                                ->get('companies');
               
        }else{
            $query = $this->db->order_by('id ASC')
                                ->limit(20,1)
                                ->get('companies');
        }

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
        else
        {
            return FALSE;
        }
    }

    public function check_last_page($page){
            $start = intval($page+1)*20;
            
           $query = $this->db->order_by('id ASC')
                                ->limit(20,$start)
                                ->get('companies');

        if ($query->num_rows() > 0)
        {
            return FALSE;
        }
        else
        {
            return TRUE;
        }
    }

    public function save_company($data)
    {
        $this->db->set($data);
        if ($this->db->insert('companies')){
            return 1;
        }
        return 0;
    }

    public function update_company($id, $data)
    {
        if (isset($data['cif'])){
            unset($data['cif']);
        }
        if (isset($data['email'])){
            unset($data['email']);
        }
        if (isset($data['date'])){
            unset($data['date']);
        }
        $this->db->set($data)
                    ->where('id', $id);
        if ($this->db->update('companies')){
            return 1;
        }
        return 0;
    }

    public function delete_company($id)
    {
        $this->db->where('id', $id);
        if ($this->db->delete('companies')){
            return 1;
        }
        return 0;
    }


    public function check_field_companies($field, $data){
        $query = $this->db->where($field, $data)
                        ->get('companies');

        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
        else
        {
            return FALSE;
        }
    }
     public function get_company($id){
        $query = $this->db->select('c.*, f.id as favorito')
                          ->from('companies c')
                          ->where('c.id', $id)
                          ->join('favorites f', 'c.id = f.id_company', 'left')
                          ->get();

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return FALSE;
        }
    }

    public function update_interfaces($table, $data)
    {
        $where = "id = 1";

        return $this->db->update($table, $data, $where);
    }

    public function fetch_data($id_user, $query, $numero_item, $start=false )
    {
        $this->db->select("c.*, f.id as favorito")
                ->from('companies c')
                ->where('c.id_user', $id_user)
                ->join('favorites f', 'c.id = f.id_company', 'left');
        if($query != '')
        {
            $this->db->like('c.description', $query);
        }
        $this->db->order_by('c.id', 'DESC');
        if ($start){
            $this->db->limit($numero_item,$start);
        }else{
            $this->db->limit($numero_item,1);
        }

        // echo $this->db->get_compiled_select('companies'); die();
        $query = $this->db->get();
        if ($query->num_rows() > 0)
        {
            return $query->result();
        }
        else
        {
            return FALSE;
        }
    }

    public function get_all_companies($id_user, $search){

        $this->db->select('COUNT(*) as total')
                 ->where('id_user', $id_user);
        if ($search != '')
        {
            $this->db->like('description', $search);

        }
        $query = $this->db->get('companies');

        if ($query->num_rows() > 0)
        {
            return $query->result_array();
        }
        else
        {
            return FALSE;
        }
    }

    public function update_company_heart($id, $like)
    {
        if ($like == 1){
            $this->db->where('id_company', $id);
            if ($this->db->delete('favorites')){  
                return 0;
            }
        }else{
            $this->db->set('id_user', $_SESSION['id_user'])
                      ->set('id_company', $id);
            if ($this->db->insert('favorites')){
                return 1;          
            }
        }
        
    }
}
