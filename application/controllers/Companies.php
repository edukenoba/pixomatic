<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Companies extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();

		/* Load :: Common */
		$this->load->model('Companies_model', 'mcompanies');

	}

	/*
	* Cargo la lista de empresas
	*/
	public function index()
	{
		
		$this->load->view('companies/list');
		// $this->load->view('list', $this->data);
	}

	/*
	* Cargo la vista para crear Empresas
	*/
	public function create($params = false)
	{
		self::form_company($params);

		$this->load->view('companies/create', $this->data);
	}


	/*
	* Creo el formulario de creación de empresas
	*/
	private function form_company($params = false){
		
		$this->data['name'] = array(
			'name'  => 'name',
			'id'    => 'name',
			'type'  => 'text',
			'class' => 'form-control',
			'required' => 'required',
			'value' => isset($params['name']) ? $params['name'] : '',
		);
		$this->data['cif'] = array(
			'name'  => 'cif',
			'id'    => 'cif',
			'type'  => 'text',
			'class' => 'form-control',
			'required' => 'required',
			'value' => isset($params['cif']) ? $params['cif'] : '', 
		);
		$this->data['shortdesc'] = array(
			'name'  => 'shortdesc',
			'id'    => 'shortdesc',
			'type'  => 'textarea',
			'rows'  => '2',
			'class' => 'form-control rounded-0',
			'value' => isset($params['shortdesc']) ? $params['shortdesc'] : '', 
		);
		$this->data['description'] = array(
			'name'  => 'description',
			'id'    => 'description',
			'type'  => 'textarea',
			'rows'  => '4',
			'class' => 'form-control rounded-0',
			'required' => 'required',
			'value' => isset($params['description']) ? $params['description'] : '', 
		);
		$this->data['email'] = array(
			'name'  => 'email',
			'id'    => 'email',
			'type'  => 'email',
			'class' => 'form-control',
			'required' => 'required',
			'value' => isset($params['email']) ? $params['email'] : '', 
		);
		$this->data['ccc'] = array(
			'name'  => 'ccc',
			'id'    => 'ccc',
			'type'  => 'text',
			// 'pattern' => '^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$',
			'class' => 'form-control text-uppercase',
			'value' => isset($params['ccc']) ? $params['ccc'] : '', 
		);
		$this->data['status'] = array(
			'name'  => 'status',
			'id'    => 'status',
			'type'  => 'text',
			'class' => 'form-control',
		);
		$this->data['logo'] = array(
			'name'  => 'logo',
			'id'    => 'logo',
			'type'  => 'text',
			'class' => 'form-control',
			'required' => 'required',
			'value' => isset($params['logo']) ? $params['logo'] : '', 
		);
	}

	/*
	* Guardo los datos de una mpresa creada
	*/
	public function save()
	{
		$params = $this->input->post();
		
		check_form($params);

		$check_name_exist = $this->mcompanies->check_field_companies('name', $params['name']);
		if ($check_name_exist){
			unset($params['name']);
			$this->session->set_flashdata('error', 'Ya existe una empresa con este nombre');
			self::create($params);
			return;
		}

		$check_cif_exist = $this->mcompanies->check_field_companies('cif', $params['cif']);
		if ($check_cif_exist){
			unset($params['cif']);
			$this->session->set_flashdata('error', 'Ya existe una empresa con este CIF');
			self::create($params);
			return;
		}
	
		if ($this->mcompanies->save_company($params)){
			$this->session->set_flashdata('success', 'Empresa añadida correctamente');
		}else{
			$this->session->set_flashdata('error', 'Error al añadir la Empresa');
		}

		redirect('/companies/', 'refresh');

	}


	/*
	* Cargo la vista de Editar
	*/
	public function edit($id_company)
	{
		$data_company = $this->mcompanies->get_company($id_company);
		$this->data['favorito'] = $data_company[0]['favorito']; 
		self::form_company($data_company[0]);
		$this->data['id'] = $id_company;
		$this->load->view('companies/edit', $this->data);
	}

	/*
	* Actualizo los datos que permito actualizar
	*/
	public function update($id)
	{
		$params = $this->input->post();
		self::check_form($params, 'edit');

		if (isset($params['cif']) || isset($params['email']) || isset($params['date'])){
			unset($params['cif']);
			unset($params['email']);
			unset($params['date']);
			$this->session->set_flashdata('error', 'No puedes modificar los campos bloqueados');
			redirect('/companies/edit/'.$id, 'refresh');

		}else{
			if ($this->mcompanies->update_company($id, $params)){
				$this->session->set_flashdata('success', 'Empresa acualizada correctamente');
			}else{
				$this->session->set_flashdata('error', 'Error al acualizar la Empresa');
			}
		}

		redirect('/companies/', 'refresh');
	}

	public function delete($id){
		if ($this->mcompanies->delete_company($id)){
			$this->session->set_flashdata('success', 'Empresa eliminada correctamente');
		}else{
			$this->session->set_flashdata('error', 'Error al eliminar la Empresa');
		}
		redirect('/companies/', 'refresh');
	}

	/*
	* Valido el formulario
	*/
	private function check_form($params, $edit = false)
	{
		$error = 0;
		if ($params['name'] == ''){
			$this->session->set_flashdata('error', 'Nombre inválido');
			self::create($params);
			return;
		}
		
		if (!$edit)
		{
			if ($params['cif'] == ''){
				$this->session->set_flashdata('error', 'CIF incorrecto');
				self::create($params);
				return;
			}else if(!ctype_digit($params['cif'][0])){
				$this->session->set_flashdata('error', 'El primer caracter debe ser número');
				self::create($params);
				return;
			}else if(intval(strlen($params['cif']) != 9)){
				$this->session->set_flashdata('error', 'Máximo 9 caracteres');
				self::create($params);
				return;
			}
			if ($params['email'] == ''){
				$this->session->set_flashdata('error', 'Nombre inválido');
				self::create($params);
				return;
			}
		}

		if ($params['description'] == ''){
			self::create($params);
			return;
		}
		$ccc = str_split($params['ccc']);
		if ($params['ccc'] != ''){
			$pais = strtoupper($ccc[0].$ccc[1]);
			if ($pais == 'AT' 
			 || $pais == 'FI' 
			 || $pais == 'IT' 
			 || $pais == 'GB' 
			 || $pais == 'BE' 
			 || $pais == 'FR' 
			 || $pais == 'LV' 
			 || $pais == 'CZ' 
			 || $pais == 'BG' 
			 || $pais == 'GI' 
			 || $pais == 'LI' 
			 || $pais == 'RO' 
			 || $pais == 'CY' 
			 || $pais == 'GR' 
			 || $pais == 'LT' 
			 || $pais == 'SM' 
			 || $pais == 'HR' 
			 || $pais == 'GL' 
			 || $pais == 'LU' 
			 || $pais == 'SE' 
			 || $pais == 'DK' 
			 || $pais == 'NL' 
			 || $pais == 'MT' 
			 || $pais == 'CH' 
			 || $pais == 'SK' 
			 || $pais == 'HU' 
			 || $pais == 'MC' 
			 || $pais == 'SI' 
			 || $pais == 'IE' 
			 || $pais == 'NO' 
			 || $pais == 'ES' 
			 || $pais == 'IS' 
			 || $pais == 'PL'){
			}else{
				$this->session->set_flashdata('error', 'Código país inválido');
				self::create($params);
				return;

			}
			$digitos = '';
			
			foreach ($ccc as $key => $value) {
				if ($key != 0 && $key != 1){
					$digitos = $digitos . $value;
				}
			}
			if (!ctype_digit($digitos)){
				$this->session->set_flashdata('error', 'Despues del codigo pais debe de haber solo numeros');
				self::create($params);
				return;
			}
			if (strlen($digitos) != 22){
				$this->session->set_flashdata('error', 'CCC incorrecto');
				self::create($params);
				return;
			}
			
		}
		
		if ($params['logo'] == ''){
			$this->session->set_flashdata('error', 'Nombre inválido');
			self::create($params);
			return;
		}else{
			$url = $params['logo'];

			$urlparts= parse_url($url);

			$scheme = $urlparts['scheme'];

			if ($scheme !== 'https') {
				$this->session->set_flashdata('error', 'URL del logo incorrecta');
				self::create($params);
				return;
			}
		}
	}

	/*
	* Hago la busqueda de los datos y formo la tabla.
	*/
	public function fetch($page = false)
	{
		$query = '';
		if($this->input->post('query'))
		{
			$query = $this->input->post('query');
		}
		$total_companies = $this->mcompanies->get_all_companies($_SESSION['id_user'], $query);
		$data = $this->mcompanies->fetch_data($_SESSION['id_user'], $query, 22);
		$this->data['query'] = $query;
		$buttons_page = '';
		if ($total_companies[0]['total'] > 0) {
		    $page = false;

		    //examino la pagina a mostrar y el inicio del registro a mostrar
		    if (isset($_GET["page"])) {
		        $page = $_GET["page"];
		    }

		    $total_pages = ceil($total_companies[0]['total'] / 20);
		    $url = parse_url($_SERVER['REQUEST_URI']);
			if (isset($url['query'])){
				parse_str($url['query'], $params);
				if (isset($params['page']) && $params['page'] != 0){
					$page = $params["page"];
				}
			}

		    if (!$page) {
		        $start = 0;
		        $page = 1;
		    } else {
		        $start = ($page - 1) * 20;
		    }
		    
		    $data = $this->mcompanies->fetch_data($_SESSION['id_user'], $query, 20, $start);

		    $buttons_page = '<div class="row justify-content-center">';
		    $buttons_page .= '<div class="col-auto d-flex align-items-center">';
		    $buttons_page .= '<ul class="pagination">';

		    if ($total_pages > 1) {
		        if ($page != 1) {
		        	$previus_page = $page-1;
		            $buttons_page .= '<li class="page-item"><a class="page-link" onClick="gotopage('.$previus_page.', \''.$query.'\');"><span aria-hidden="true">&laquo;</span></a></li>';
		        }

		        for ($i=1;$i<=$total_pages;$i++) {
		            if ($page == $i) {
		                $buttons_page .= '<li class="page-item active"><a class="page-link" href="#">'.$page.'</a></li>';
		            } else {
		                $buttons_page .= '<li class="page-item"><a class="page-link" onClick="gotopage('.$i.', \''.$query.'\');">'.$i.'</a></li>';
		            }
		        }

		        if ($page != $total_pages) {
		        	$next_page = $page+1;
		            $buttons_page .= '<li class="page-item"><a class="page-link" onClick="gotopage('.$next_page.', \''.$query.'\');"><span aria-hidden="true">&raquo;</span></a></li>';
		        }
		    }
		    $buttons_page .= '</ul>';
		    $buttons_page .= '</div>';
		    $buttons_page .= '</div>';
		}

		echo self::table_companies($data, $buttons_page);
	}

	/*
	* Formo la tabla de Empresas
	*/
	private function table_companies($data, $buttons_page = false)
	{
		$output = '';
		$output .= '<div class="table-responsive">
						<table id="tablaCompanies" class="table table-striped table-hover">
							<thead>
								<tr>
									<th width="20%">Logo</th>
									<th width="10%">Name</th>
									<th width="15%">Shortdesc</th>
									<th width="20%">Description</th>
									<th width="10%">CCC</th>
									<th width="5%"">Status</th>
									<th width="10%"" class="no-sort text-right">Acción</th>
								</tr>
							</thead>
		';
		if($data != false && count($data) > 0)
		{
			foreach($data as $row)
			{
				if ($row->status){
					$status = '<i class="fas fa-check-circle fa-2x" style="color: green"></i>';
				}else{
					$status = '<i class="fas fa-times fa-2x" style="color: red"></i>';
				}

				if ($row->favorito != null)
				{
					$heart = '<i class="fas fa-heart fa-2x cursor-hover mt-3"></i>';
				}else{
					$heart = '<i class="far fa-heart fa-2x cursor-hover mt-3"></i>';
				}

				$output .= '
					<tbody>
						<tr>
							<td><img src="'.$row->logo.'" class="img-cat img-fluid d-block mx-auto"</td>
							<td>'.$row->name.'</td>
							<td>'.$row->shortdesc.'</td>
							<td>'.$row->description.'</td>
							<td>'.$row->ccc.'</td>
							<td align="center">'.$status.'</td>
							<td align="center">
								<a role="button" href="'. base_url('companies/edit/'.$row->id).'" class="btn btn-default btn-resize mr-auto">Editar</a><br>
								'.$heart.'
							</td>
						</tr>
					</tbody>
				';
			}
		}
		else
		{
			$output .= '<tr>
							<td colspan="10">No Data Found</td>
						</tr>';
		}
		$output .= '</table>';
		$output .= '</div>';

		if ($buttons_page){
			$output .= $buttons_page;
		}
		return $output;
	}

	/*
	* Like o Dislike de la empresa
	*/
	public function like($id)
	{
		if ($this->input->post())
		{
			$params = $this->input->post();
		}

		if ($this->mcompanies->update_company_heart($id, $params['like']))
		{
			$this->session->set_flashdata('success', '¡Esta empresa te gusta!');
			echo '1';
		}else{
			$this->session->set_flashdata('warning', '¡Esta empresa ya no te gusta!');
			echo '0';
		}
	}

}
