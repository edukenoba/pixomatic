<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Owners extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct()
	{
		parent::__construct();

		/* Load :: Common */
		$this->load->model('Owners_model', 'mowners');

	}

	public function index()
	{
		$url = parse_url($_SERVER['REQUEST_URI']);
		if (isset($url['query'])){
			parse_str($url['query'], $params);
			if (isset($params['page']) && $params['page'] != 0){
				$page_url = $params["page"];
			}
		}

		if (isset($page_url)){
			$get_data = $this->mowners->callAPI('POST', 'https://gorest.co.in/public/v1/users?page='.$page_url, false);
		}else{
			$get_data = $this->mowners->callAPI('POST', 'https://gorest.co.in/public/v1/users?page=1', false);
		}
		$response = json_decode($get_data, true);
		$data = $response['data'];
		// var_dump($response['meta']['pagination']['page']); die();
		// var_dump($response['meta']['pagination']['pages']); die();
		// var_dump($response['meta']['pagination']['limit']); die();
		$page = $response['meta']['pagination']['page'];
		$pages = $response['meta']['pagination']['pages'];
		$limit = $response['meta']['pagination']['limit'];
	    $buttons_page = '<div class="row justify-content-center">';
	    $buttons_page .= '<div class="col-auto d-flex align-items-center">';
	    $buttons_page .= '<ul class="pagination">';

	    if ($pages > 1) {
	        if ($page != 1) {
	        	$previus_page = $page-1;
	            $buttons_page .= '<li class="page-item"><a class="page-link" href="'.base_url('owners?page='.$page).'"><span aria-hidden="true">&laquo;</span></a></li>';
	        }

	        for ($i=1;$i<=$pages;$i++) {
	            if ($page == $i) {
	                $buttons_page .= '<li class="page-item active"><a class="page-link" href="#">'.$page.'</a></li>';
	            } else {
	                $buttons_page .= '<li class="page-item"><a class="page-link" href="'.base_url('owners?page='.$i).'">'.$i.'</a></li>';
	            }
	        }

	        if ($page != $pages) {
	        	$next_page = $page+1;
	            $buttons_page .= '<li class="page-item"><a class="page-link" href="'.base_url('owners?page='.$next_page).'"><span aria-hidden="true">&raquo;</span></a></li>';
	        }
	    }
	    $buttons_page .= '</ul>';
	    $buttons_page .= '</div>';
	    $buttons_page .= '</div>';

		$this->data['table_owners'] = self::table_owners($data, $buttons_page);

		$this->load->view('owners/list' ,$this->data);
	}

	public function view($id_user)
	{
		if ($id_user != ''){
			$user = $this->mowners->callAPI('POST', 'https://gorest.co.in/public/v1/users/'.$id_user, false);
			$posts = $this->mowners->callAPI('POST', 'https://gorest.co.in//public/v1/users/'.$id_user.'/posts', false);
			$comments = $this->mowners->callAPI('POST', 'https://gorest.co.in//public/v1/users/'.$id_user.'/comments', false);
			$this->data['user'] = json_decode($user)->data;
			$this->data['posts'] = json_decode($posts)->data;
			// var_dump($this->data['posts']); die();
			$this->data['comments'] = json_decode($comments)->data;
		}else{
			$this->session->set_flashdata('error', 'Ha habido un error');

		}
		$this->load->view('owners/view' ,$this->data);
	}
	public function post($id_user, $id_post)
	{
		if ($id_post != ''){
			$posts = $this->mowners->callAPI('POST', 'https://gorest.co.in//public/v1/users/'.$id_user.'/posts', false);
			$this->data['posts'] = json_decode($posts)->data;
			if (count($this->data['posts']) > 0){
				foreach ($this->data['posts'] as $key => $value) {
					if ($id_post == $value->id){
						$post = $value;
					}
				}
			}
			// var_dump($post); die();
			$this->data['post'] = $post;
			// $this->data['comments'] = json_decode($comments)->data;
		}else{
			$this->session->set_flashdata('error', 'Ha habido un error');

		}
		$this->load->view('owners/post' ,$this->data);
	}

	public function fetch($page = false, $limit)
	{
		//Hago la busqueda de los datos y formo la tabla.
		$query = '';
		if($this->input->post('query'))
		{
			$query = $this->input->post('query');
		}
		$total_companies = $this->mcompanies->get_all_companies($query);
		$data = $this->mcompanies->fetch_data($query, 22);
		$this->data['query'] = $query;
		$buttons_page = '';
		if ($total_companies[0]['total'] > 0) {
		    $page = false;

		    //examino la pagina a mostrar y el inicio del registro a mostrar
		    if (isset($_GET["page"])) {
		        $page = $_GET["page"];
		    }

		    $total_pages = ceil($total_companies[0]['total'] / 20);
		    // var_dump($total_companies[0]['total']); die();
		    $url = parse_url($_SERVER['REQUEST_URI']);
			if (isset($url['query'])){
				parse_str($url['query'], $params);
				if (isset($params['page']) && $params['page'] != 0){
					$page = $params["page"];
				}
			}

		    if (!$page) {
		        $start = 0;
		        $page = 1;
		    } else {
		        $start = ($page - 1) * 20;
		    }
		  
		    $data = $this->mcompanies->fetch_data($query, 20, $start);


	
		    
		}


		echo self::table_companies($data, $buttons_page);
	}

	private function table_owners($data, $buttons_page = false)
	{
		$output = '';
		$output .= '<div class="table-responsive">
						<table id="tablaCompanies" class="table table-striped table-hover">
							<thead>
								<tr>
									<th width="25%">Name</th>
									<th width="25%">Email</th>
									<th width="25%">Gender</th>
									<th width="25%" class="text-center">Status</th>
									<th width="25%" class="text-center">View</th>
								</tr>
							</thead>
		';
		if($data != false && count($data) > 0)
		{
			foreach($data as $row)
			{
				if ($row['status'] == 'active'){
					$status = '<i class="fas fa-check-circle fa-2x" style="color: green"></i>';
				}else{
					$status = '<i class="fas fa-times fa-2x" style="color: red"></i>';
				}

				$output .= '
					<tbody>
						<tr>
							<td>'.$row['name'].'</td>
							<td>'.$row['email'].'</td>
							<td>'.$row['gender'].'</td>
							<td align="center">'.$status.'</td>
							<td align="center"><a class="text-dark" href="'.base_url('owners/view/'.$row['id']).'"><i class="far fa-eye fa-2x"></i></a></td>
						</tr>
					</tbody>
				';
			}
		}
		else
		{
			$output .= '<tr>
							<td colspan="10">No Data Found</td>
						</tr>';
		}
		$output .= '</table>';
		$output .= '</div>';

		if ($buttons_page){
			$output .= $buttons_page;
		}
		return $output;
	}

	private function callAPI($method, $url, $data){
			   
		$curl = curl_init();
		$headers = array(
		    'Accept:application/json',
		    'Content-Type: application/json',
		    'Authorization: Bearer bdcf2f6c9a6ca4950f613d56bb16bbaf6a56bd3bc000fb130ad2c16eb36a99d8',
		);

		curl_setopt_array($curl, array(
		  CURLOPT_URL => $url,
	      // curl_setopt($curl, CURLOPT_POST, 1);
		  CURLOPT_HTTPHEADER => $headers,
		  CURLOPT_RETURNTRANSFER => true,
		));


	   // EXECUTE:
	   $result = curl_exec($curl);
	   if(!$result){die("Connection Failure");}
	   curl_close($curl);
	   return $result;
	}

}
