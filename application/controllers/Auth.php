<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Auth extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();

		/* Load :: Common */
		$this->load->model('Owners_model', 'mowners');

	}

	public function login()
	{
		if (isset($_SESSION['id_user'])){
			redirect('/companies/', 'refresh');
		}else{
			if ($this->input->post()){
				$email=$this->input->post();
				$users_actives = $this->mowners->get_all_user();
				foreach ($users_actives as $key => $value) {
					if ($value->email == $email['email']){
						$_SESSION['id_user'] = $value->id;
						$_SESSION['username'] = $value->name;
						$this->session->set_flashdata('success', '¡Bienvenido '.$value->name.'!');
						redirect('/companies/', 'refresh');
					}else{
						$this->session->set_flashdata('error', 'Login incorrecto');
					}
				}
			}
		}

		$this->load->view('auth/login');
	}
	public function logout()
	{
		unset($_SESSION['id_user']);
		$this->load->view('auth/login');
	}

}
