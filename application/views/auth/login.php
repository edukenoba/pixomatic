<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<head>
    <?php $this->load->view('_partials/head') ?>
</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <?= $this->config->item('site_name') ?>
        </div>
        <!-- /.login-logo -->
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">
                    Selamat datang, <br>
                    silahkan login untuk melanjutkan
                </p>
            <?php echo isset($message) ? $message : ''; ?>

            <?php echo form_open(base_url('auth/login/'), array('class' => 'form-horizontal', 'id' => 'form-login')); ?>
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" name="email" placeholder="Email" autofocus autocomplete="email">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-email"></span>
                            </div>
                        </div>
                    </div>
                  
                    <div class="row">
                        <div class="col-8">
                 
                        </div>
                        <!-- /.col -->
                        <div class="col-4">
                            <button type="submit" class="btn btn-primary btn-block">Sign In</button>
                        </div>
                        <!-- /.col -->
                    </div>
                <?php echo form_close(); ?>
            </div>
            <!-- /.login-card-body -->
        </div>
    </div>
    <!-- /.login-box -->

    <?php $this->load->view('_partials/js.php') ?>
</body>

</html>