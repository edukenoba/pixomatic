<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<head>
	<?php $this->load->view('_partials/head') ?>
</head>

<body class="hold-transition sidebar-mini layout-fixed">
	<div class="wrapper">

		<!-- Main Sidebar Container -->
		<?php $this->load->view('_partials/sidebar_main.php') ?>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<div class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-sm-6 col-12">
							<h1 class="m-0 text-dark">View post</h1>
						</div>
						<div class="col-sm-6 col-12 text-right" id="div-heart">
							
							
						</div><!-- /.col -->
						
					</div><!-- /.row -->
				</div><!-- /.container-fluid -->
			</div>
			<!-- /.content-header -->

			<!-- Main content -->
			 <section class="content">
		        <div class="row justify-content-center">
		            <div class="col-md-12">
		                <h2 class="font-weight-bold"><?php echo $post->title; ?></h2>
		             </div>
		             <div class="col-md-12">
		                <p><?php echo $post->body; ?></p>
		             </div>
		        </div>
		    </section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<?php $this->load->view('_partials/footer.php') ?>

		<!-- Control Sidebar -->
		<?php $this->load->view('_partials/sidebar_control.php') ?>
		<!-- /.control-sidebar -->
	</div>
	<!-- ./wrapper -->

	<?php $this->load->view('_partials/js.php') ?>
	<?php $this->load->view('companies/js_companies.php') ?>
	<?php $this->load->view('companies/js_edit_companies.php') ?>
</body>

</html>