<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<head>
	<?php $this->load->view('_partials/head') ?>
</head>

<body class="hold-transition sidebar-mini layout-fixed">
	<div class="wrapper">

		<!-- Main Sidebar Container -->
		<?php $this->load->view('_partials/sidebar_main.php') ?>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<div class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-sm-6 col-12">
							<h1 class="m-0 text-dark">View owner</h1>
						</div>
						<div class="col-sm-6 col-12 text-right" id="div-heart">
							
							
						</div><!-- /.col -->
						
					</div><!-- /.row -->
				</div><!-- /.container-fluid -->
			</div>
			<!-- /.content-header -->

			<!-- Main content -->
			 <section class="content">
		        <div class="row">
		            <div class="col-md-12">
		                 <div class="box">
		                    <div class="box-body">
                            	<div class="row py-4 justify-content-center">
	                                <div class="col-sm-1 font-weight-bold">
	                                	<span>Name</span>
	                                </div>
	                                <div class="col-sm-10 bg-white">
	                                    <?php echo $user->name; ?>
	                                </div>
	                            </div>
                            	<div class="row py-4 justify-content-center">
	                                <div class="col-sm-1 font-weight-bold">
	                                	<span>Email</span>
	                                </div>
	                                <div class="col-sm-10 bg-white">
	                                    <?php echo $user->email; ?>
	                                </div>
	                            </div>
                            	<div class="row py-4 justify-content-center">
	                                <div class="col-sm-1 font-weight-bold">
	                                	<span>Gender</span>
	                                </div>
	                                <div class="col-sm-10 bg-white">
	                                    <?php echo $user->gender; ?>
	                                </div>
	                            </div>
	                            <div class="row py-4 justify-content-center">
	                                <div class="col-sm-1 font-weight-bold">
	                                	<span>Status</span>
	                                </div>
	                                <div class="col-sm-10 bg-white">
	                                    <?php if ($user->status == 'active'): ?>
	                                    	<span class="bg-success text-white"><?php echo $user->status; ?> </span>
	                                    <?php else: ?>
	                                    	<span class="bg-danger text-white"><?php echo $user->status; ?> </span>
	                                    <?php endif; ?>

	                                    	
	                                    
	                                </div>
	                            </div>
		                    </div>
		                </div>
		             </div>
		        </div>
		        <div class="row pt-5 justify-content-center">
		        	<div class="col-12">
		        		<h2>Posts</h3>
		        	</div>
		        	<?php foreach ($posts as $key => $value): ?>
		        	<div class="col-11 pt-3">
		        			<h4 class="font-weight-bold"><a class="text-dark" href="<?php echo base_url('owners/post/'.$user->id.'/'.$value->id);?>"><?php echo $value->title; ?></a></h4>
		        			<p><?php echo $value->body; ?></p>
		        	</div>
		        		<?php endforeach; ?>
		        </div>
		    </section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<?php $this->load->view('_partials/footer.php') ?>

		<!-- Control Sidebar -->
		<?php $this->load->view('_partials/sidebar_control.php') ?>
		<!-- /.control-sidebar -->
	</div>
	<!-- ./wrapper -->

	<?php $this->load->view('_partials/js.php') ?>
	<?php $this->load->view('companies/js_companies.php') ?>
	<?php $this->load->view('companies/js_edit_companies.php') ?>
</body>

</html>