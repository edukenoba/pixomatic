<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<head>
	<?php $this->load->view('_partials/head') ?>
</head>

<body class="hold-transition sidebar-mini layout-fixed">
	<div class="wrapper">

		<!-- Main Sidebar Container -->
		<?php $this->load->view('_partials/sidebar_main.php') ?>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<div class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-sm-6">
							<h1 class="m-0 text-dark">Owners</h1>
						</div><!-- /.col -->
						<!-- <div class="col-sm-6">
							<ol class="breadcrumb float-sm-right">
								<li class="breadcrumb-item"><a href="<?= base_url() ?>">Home</a></li>
								<li class="breadcrumb-item active">Template</li>
							</ol>
						</div> --><!-- /.col -->
					</div><!-- /.row -->
				</div><!-- /.container-fluid -->
			</div>
			<!-- /.content-header -->

			<!-- Main content -->
			<section class="content pb-3">
				<div class="row">
					<div class="col-md-12">
						 <div class="box">
							<div class="box-body">
								<div id="result"><?php echo $table_owners; ?></div>
								<!-- <table id="tablaCompanies" class="table table-striped table-hover">
									<thead>
										<tr>
											<th data-priority="2">Logo</th>
											<th data-priority="3">Name</th>
											<th data-priority="2">CIF</th>
											<th data-priority="2">Shortdesc</th>
											<th data-priority="3">Description</th>
											<th data-priority="1">Email</th>
											<th data-priority="1">CCC</th>
											<th data-priority="1">Date</th>
											<th data-priority="3">Status</th>
											<th data-priority="1" class="no-sort text-right">Acción</th>
										</tr>
									</thead>
									<tbody>
										<?php foreach ( $companies as $company ): ?>
											<tr>
												<td><img class="img-fluid d-block mx-auto img-cat" src="<?php echo $company->logo; ?>" /></td>
												<td><?php echo htmlspecialchars($company->name, ENT_QUOTES, 'UTF-8'); ?></td>
												<td><?php echo htmlspecialchars($company->cif, ENT_QUOTES, 'UTF-8'); ?></td>
												<td><?php echo htmlspecialchars($company->shortdesc, ENT_QUOTES, 'UTF-8'); ?></td>
												<td><?php echo htmlspecialchars($company->description, ENT_QUOTES, 'UTF-8'); ?></td>
												<td><?php echo htmlspecialchars($company->email, ENT_QUOTES, 'UTF-8'); ?></td>
												<td><?php echo htmlspecialchars($company->ccc, ENT_QUOTES, 'UTF-8'); ?></td>
												<td><?php echo htmlspecialchars($company->date, ENT_QUOTES, 'UTF-8'); ?></td>
												<td align="center"><?php echo ($company->status) ? '<i class="fas fa-check-circle" style="color: green"></i>' : '<i class="fas fa-times" style="color: red"></i>'; ?></td>
												<td class="text-right">
													<a role="button" href="<?php echo base_url('companies/edit/'.$company->id); ?>" class="btn btn-default btn-resize"><?php echo 'Editar'; ?></a>
												</td>
											</tr>
										<?php endforeach; ?>
									</tbody>
								</table> -->
								<!-- <div class="row justify-content-center" id="page_buttons">
									<div class="col-2 text-center">
										<?php if($previus_page != -1): ?>
											<a href="<?php echo base_url('companies/?page='.$previus_page); ?>"><i class="fas fa-arrow-left"></i></a>
										<?php endif; ?>

									</div>
									<div class="col-2 text-center">
										<span class="py-2 px-3 bg-info" style="border-radius: 25px"><?php echo $current_page; ?></span>
									</div>
									<div class="col-2 text-center">
										<?php if($next_page != -1): ?>
											<a href="<?php echo base_url('companies/?page='.$next_page); ?>"><i class="fas fa-arrow-right"></i></a>

										<?php endif; ?>
									</div>
								</div> -->
							</div>
						</div>
					 </div>
				</div>
			</section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<?php $this->load->view('_partials/footer.php') ?>

		<!-- Control Sidebar -->
		<?php $this->load->view('_partials/sidebar_control.php') ?>
		<!-- /.control-sidebar -->
		
	</div>
	<!-- ./wrapper -->

	<?php $this->load->view('_partials/js.php') ?>
	<?php $this->load->view('owners/js_owners_list.php') ?>
</body>

</html>