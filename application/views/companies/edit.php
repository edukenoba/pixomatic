<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html>

<head>
	<?php $this->load->view('_partials/head') ?>
</head>

<body class="hold-transition sidebar-mini layout-fixed">
	<div class="wrapper">

		<!-- Main Sidebar Container -->
		<?php $this->load->view('_partials/sidebar_main.php') ?>

		<!-- Content Wrapper. Contains page content -->
		<div class="content-wrapper">
			<!-- Content Header (Page header) -->
			<div class="content-header">
				<div class="container-fluid">
					<div class="row mb-2">
						<div class="col-sm-6 col-12">
							<h1 class="m-0 text-dark">Editar Empresa</h1>
						</div>
						<div class="col-sm-6 col-12 text-right" id="div-heart">
							
							<?php if($favorito != null): ?>
								<i class="fas fa-heart fa-4x cursor-hover heart" data-heart="1"></i>
							<?php else: ?>
								<i class="far fa-heart fa-4x cursor-hover heart" data-heart="0"></i>
							<?php endif; ?>
						</div><!-- /.col -->
					</div><!-- /.row -->
				</div><!-- /.container-fluid -->
			</div>
			<!-- /.content-header -->

			<!-- Main content -->
			 <section class="content">
		        <div class="row">
		            <div class="col-md-12">
		                 <div class="box">
		                    <div class="box-body">
		                        <?php echo isset($message) ? $message : ''; ?>

		                        <?php echo form_open(base_url('companies/update/'.$id), array('class' => 'form-horizontal', 'id' => 'form-edit_companies')); ?>
		                            <div class="form-group">
		                            	<div class="row justify-content-center">
			                                <div class="col-sm-1 font-weight-bold">
			                                	<span>Name</span>
			                                </div>
			                                <div class="col-sm-10">
			                                    <?php echo form_input($name); ?>
			                                </div>
			                            </div>
		                            </div>
		                            <div class="form-group">
		                            	<div class="row justify-content-center">
			                                <div class="col-sm-1 font-weight-bold">
			                                	<span>CIF</span>
			                                </div>
			                                <div class="col-sm-10">
			                                    <?php echo form_input($cif); ?>
			                                </div>
			                            </div>
		                            </div>
		                            <div class="form-group">
		                            	<div class="row justify-content-center">
			                                <div class="col-sm-1 font-weight-bold">
			                                	<span>Shortdesc</span>
			                                </div>
			                                <div class="col-sm-10">
			                                    <?php echo form_textarea($shortdesc); ?>
			                                </div>
			                            </div>
		                            </div>
		                            <div class="form-group">
		                            	<div class="row justify-content-center">
			                                <div class="col-sm-1 font-weight-bold">
			                                	<span>Description</span>
			                                </div>
			                                <div class="col-sm-10">
			                                    <?php echo form_textarea($description); ?>
			                                </div>
			                            </div>
		                            </div>
		                            <div class="form-group">
		                            	<div class="row justify-content-center">
			                                <div class="col-sm-1 font-weight-bold">
			                                	<span>Email</span>
			                                </div>
			                                <div class="col-sm-10">
			                                    <?php echo form_input($email); ?>
			                                </div>
			                            </div>

		                            </div>
		                            <div class="form-group">
		                            	<div class="row justify-content-center">
			                                <div class="col-sm-1 font-weight-bold">
			                                	<span>CCC</span>
			                                </div>
			                                <div class="col-sm-10">
			                                    <?php echo form_input($ccc); ?>
			                                </div>
			                            </div>
		                            </div>
		                            <div class="form-group">
		                            	<div class="row justify-content-center">
			                                <div class="col-sm-1 font-weight-bold">
			                                	<span>Status</span>
			                                </div>
			                                <div class="col-sm-10">
			                                    <div class="custom-control custom-radio">
			                                      <input type="radio" id="activo" name="status" class="custom-control-input" value="1">
			                                      <label class="custom-control-label" for="activo">Activo</label>
			                                    </div>
			                                    <div class="custom-control custom-radio">
			                                      <input type="radio" id="inactivo" name="status" class="custom-control-input" value="0">
			                                      <label class="custom-control-label" for="inactivo">Inactivo</label>
			                                    </div>
			                                </div>
			                            </div>
		                               
		                            </div>
		                            <div class="form-group">
		                            	<div class="row justify-content-center">
			                                <div class="col-sm-1 font-weight-bold">
			                                	<span>Logo</span>
			                                </div>
			                                <div class="col-sm-10">
			                                    <?php echo form_input($logo); ?>
			                                    <span class="btn btn-info mt-2" id="cargar-logo">Cargar Logo</span>
			                                    <div class="row justify-content-center justify-content-center">
			                                    	<div class="col-6" id="div-logo">
			                                    	</div>
			                                    </div>

			                                </div>
			                           	</div>
		                            </div>
		                            <div class="form-group">
		                            	<div class="row justify-content-center">
			                                <div class="col-sm-10">
			                                    <div class="btn-group">
			                                        <?php echo form_button(array('type' => 'submit', 'class' => 'btn btn-primary btn-flat', 'content' => 'Enviar')); ?>
			                                        <?php echo form_button(array('type' => 'reset', 'class' => 'btn btn-warning btn-flat', 'content' => 'Resetear')); ?>
			                                        <span class="btn btn-danger btn-flat" id="delete-company">Eliminar</span>
			                                        <?php echo anchor('companies','Cancelar', array('class' => 'btn btn-default btn-flat')); ?>
			                                    </div>
			                                </div>
			                            </div>
		                            </div>
		                        <?php echo form_close(); ?>
		                    </div>
		                </div>
		             </div>
		        </div>
		    </section>
			<!-- /.content -->
		</div>
		<!-- /.content-wrapper -->

		<?php $this->load->view('_partials/footer.php') ?>

		<!-- Control Sidebar -->
		<?php $this->load->view('_partials/sidebar_control.php') ?>
		<!-- /.control-sidebar -->
	</div>
	<!-- ./wrapper -->

	<?php $this->load->view('_partials/js.php') ?>
	<?php $this->load->view('companies/js_companies.php') ?>
	<?php $this->load->view('companies/js_edit_companies.php') ?>
</body>

</html>