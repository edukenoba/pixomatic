<?php
defined('BASEPATH') or exit('No direct script access allowed');

?>
<script type="text/javascript">

    $('#owners_url').removeClass('active');
    $("#companies_url").addClass('active');

    load_data('');

    function gotopage(page, query)
    {
        $.ajax({
            url:"<?php echo base_url(); ?>companies/fetch?page="+page,
            method:"POST",
            data:{query:query},
            success:function(data){
                $('#result').html(data);
            }
        })
    }

    function load_data(query)
    {
        $.ajax({
            url:"<?php echo base_url(); ?>companies/fetch",
            method:"POST",
            data:{query:query},
            success:function(data){
                $('#page_buttons').addClass('d-none');
                $('#result').html(data);
            }
        })
    }
    $( "#search_text" ).on("keyup",function(){
        var search_length = $(this).val().length;
        console.log(search_length);
        if(search_length >= 3)
        {
            $('#search').removeAttr('disabled');
        }else{
            $('#search').prop('disabled', 'disabled');
        }
      });
    

    $('#search').click(function(){
        var search = $('#search_text').val();
        if(search != '')
        {
            load_data(search);
        }
        else
        {
            load_data('');
        }
    });
</script>