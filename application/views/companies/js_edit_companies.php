<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<script type="text/javascript">
  $('#cif').prop('disabled', 'disabled');
  $('#email').prop('disabled', 'disabled');
  
var id=<?php echo $id; ?>;
$( "#delete-company" ).click(function() {
	Swal.fire({
	  title: 'Vas a eliminar esta empresa, ¿estás de acuerdo?',
	  showCancelButton: true,
	  confirmButtonText: `Eliminar`,
	}).then((result) => {
		  /* Read more about isConfirmed, isDenied below */
		  if (result.isConfirmed) {
				window.location.replace(base_url+'companies/delete/'+id);
		  }
		})
	});

	 $('.heart').click(function(){
        $.ajax({
            url:"<?php echo base_url(); ?>companies/like/"+id,
            method:"POST",
            data:{like:$(this).attr("data-heart")},
            success:function(data){
		         location.reload();      
            }
        })
    });

</script>