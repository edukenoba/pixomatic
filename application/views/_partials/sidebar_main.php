<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<aside class="main-sidebar sidebar-dark-primary elevation-4">
	<!-- Brand Logo -->
	<a href="<?php echo base_url(); ?>" class="brand-link">
		<img src="<?= site_url('assets/img/logo_cat.png') ?>" alt="<?= $this->config->item('site_name') ?>" class="brand-image img-circle elevation-3 bg-white img-fluid" style="opacity: .8">
		<span class="brand-text font-weight-light"><?= $this->config->item('site_name') ?></span>
	</a>

	<!-- Sidebar -->
	<div class="sidebar">
		<!-- Sidebar Menu -->
		<nav class="mt-2">
			<ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
				<li class="nav-item has-treeview menu-open">
					<ul class="nav nav-treeview">
						<li class="nav-item">
							<a href="<?php echo base_url('companies') ?>" id="companies_url" class="nav-link active">
								<i class="far fa-circle nav-icon"></i>
								<p>Empresas</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="<?php echo base_url('owners') ?>" id="owners_url" class="nav-link">
								<i class="far fa-circle nav-icon"></i>
								<p>Usuarios</p>
							</a>
						</li>
						<li class="nav-item">
							<a href="<?php echo base_url('auth/logout') ?>" id="owners_url" class="nav-link">
								<i class="fas fa-power-off"></i>
								<p>Desconectar</p>
							</a>
						</li>
					</ul>
				</li>
			</ul>
		</nav>
		<!-- /.sidebar-menu -->
	</div>
	<!-- /.sidebar -->
</aside>