<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<script src="<?= base_url('assets/vendor/jquery/jquery.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/bootstrap/bootstrap.bundle.min.js') ?>"></script>

<script src="<?= base_url('assets/vendor/chart.js/Chart.min.js') ?>"></script>

<script src="<?= base_url('assets/vendor/datatables-bs4/jquery.dataTables.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/datatables-bs4/dataTables.bootstrap4.min.js') ?>"></script>

<script src="<?= base_url('assets/vendor/moment/moment.min.js') ?>"></script>
<script src="<?= base_url('assets/vendor/tempusdominus-bootstrap-4/tempusdominus-bootstrap-4.min.js') ?>"></script>

<script src="<?= base_url('assets/vendor/overlayScrollbars/jquery.overlayScrollbars.min.js') ?>"></script>

<script src="<?= base_url('assets/js/adminlte.min.js') ?>"></script>

<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
<script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

<!-- Flecha para hacer scrollTop automático -->
<span id="goup" class="hidden"><i class='fas fa-arrow-circle-up fa-2x'></i></span>

<script type="text/javascript">

    $(document).ready(function(){

        <?php if($this->session->flashdata('success')){ ?>
            toastr.success("<?php echo $this->session->flashdata('success'); ?>");
        <?php }else if($this->session->flashdata('error')){  ?>
            toastr.error("<?php echo $this->session->flashdata('error'); ?>");
        <?php }else if($this->session->flashdata('warning')){  ?>
            toastr.warning("<?php echo $this->session->flashdata('warning'); ?>");
        <?php }else if($this->session->flashdata('info')){  ?>
            toastr.info("<?php echo $this->session->flashdata('info'); ?>");
        <?php } ?>

        //Flecha para hacer scrollTop automático
        $(window).scroll(function(){
            if ($(this).scrollTop() > 300)
                $('#goup').fadeIn();
            else
                $('#goup').fadeOut();
        });

        $('#goup').click(function(){
            $("html, body").animate({ scrollTop: 0 }, 200);
            return false;
        });


        

        
    });

</script>