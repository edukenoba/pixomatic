-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Versión del servidor:         5.7.31 - MySQL Community Server (GPL)
-- SO del servidor:              Win64
-- HeidiSQL Versión:             11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

-- Volcando estructura para tabla pixomaticesf.admin_preferences
CREATE TABLE IF NOT EXISTS `admin_preferences` (
  `id` tinyint(1) NOT NULL AUTO_INCREMENT,
  `user_panel` tinyint(1) NOT NULL DEFAULT '0',
  `sidebar_form` tinyint(1) NOT NULL DEFAULT '0',
  `messages_menu` tinyint(1) NOT NULL DEFAULT '0',
  `notifications_menu` tinyint(1) NOT NULL DEFAULT '0',
  `tasks_menu` tinyint(1) NOT NULL DEFAULT '0',
  `user_menu` tinyint(1) NOT NULL DEFAULT '1',
  `ctrl_sidebar` tinyint(1) NOT NULL DEFAULT '0',
  `transition_page` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla pixomaticesf.admin_preferences: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `admin_preferences` DISABLE KEYS */;
INSERT INTO `admin_preferences` (`id`, `user_panel`, `sidebar_form`, `messages_menu`, `notifications_menu`, `tasks_menu`, `user_menu`, `ctrl_sidebar`, `transition_page`) VALUES
	(1, 0, 0, 0, 0, 0, 1, 0, 0);
/*!40000 ALTER TABLE `admin_preferences` ENABLE KEYS */;

-- Volcando estructura para tabla pixomaticesf.ci_sessions
CREATE TABLE IF NOT EXISTS `ci_sessions` (
  `id` varchar(128) NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `timestamp` int(10) unsigned NOT NULL DEFAULT '0',
  `data` blob NOT NULL,
  PRIMARY KEY (`id`,`ip_address`),
  KEY `ci_sessions_timestamp` (`timestamp`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla pixomaticesf.ci_sessions: 19 rows
/*!40000 ALTER TABLE `ci_sessions` DISABLE KEYS */;
INSERT INTO `ci_sessions` (`id`, `ip_address`, `timestamp`, `data`) VALUES
	('kuj5akansjf12sdgaqcv8k9e4820qf35', '::1', 1627141363, _binary 0x5f5f63695f6c6173745f726567656e65726174657c693a313632373134313336333b637372666b65797c733a383a226b4531597033747a223b6373726676616c75657c733a32303a22364b454679585a74686c657275636b576734626e223b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b70617373776f72647c733a36303a22243261243037245365426b6e6e74705a726f723975796674566f706d7536317167306d733851763179563646472e6b514f534d2e3951686d546f3336223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c4e3b6c6173745f636865636b7c693a313632373134303634393b),
	('t423lrlfnukbnu98qva4q4ajohdi1ugo', '::1', 1627141757, _binary 0x5f5f63695f6c6173745f726567656e65726174657c693a313632373134313735373b637372666b65797c733a383a226b4531597033747a223b6373726676616c75657c733a32303a22364b454679585a74686c657275636b576734626e223b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b70617373776f72647c733a36303a22243261243037245365426b6e6e74705a726f723975796674566f706d7536317167306d733851763179563646472e6b514f534d2e3951686d546f3336223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c4e3b6c6173745f636865636b7c693a313632373134303634393b),
	('qoomaip180nhd9ec0fqeim46qog9q2ka', '::1', 1627143367, _binary 0x5f5f63695f6c6173745f726567656e65726174657c693a313632373134333336373b637372666b65797c733a383a226b4531597033747a223b6373726676616c75657c733a32303a22364b454679585a74686c657275636b576734626e223b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b70617373776f72647c733a36303a22243261243037245365426b6e6e74705a726f723975796674566f706d7536317167306d733851763179563646472e6b514f534d2e3951686d546f3336223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c4e3b6c6173745f636865636b7c693a313632373134303634393b),
	('f5r1ve1jgngrnsi3k3amjcodrr5lem2n', '::1', 1627143768, _binary 0x5f5f63695f6c6173745f726567656e65726174657c693a313632373134333736383b637372666b65797c733a383a226b4531597033747a223b6373726676616c75657c733a32303a22364b454679585a74686c657275636b576734626e223b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b70617373776f72647c733a36303a22243261243037245365426b6e6e74705a726f723975796674566f706d7536317167306d733851763179563646472e6b514f534d2e3951686d546f3336223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c4e3b6c6173745f636865636b7c693a313632373134303634393b),
	('75i6nnh55c4smm1qfuerak0tgg7htdff', '::1', 1627144080, _binary 0x5f5f63695f6c6173745f726567656e65726174657c693a313632373134343038303b637372666b65797c733a383a226b4531597033747a223b6373726676616c75657c733a32303a22364b454679585a74686c657275636b576734626e223b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b70617373776f72647c733a36303a22243261243037245365426b6e6e74705a726f723975796674566f706d7536317167306d733851763179563646472e6b514f534d2e3951686d546f3336223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c4e3b6c6173745f636865636b7c693a313632373134303634393b),
	('f98ae74n9fbfo0t29dga9hjj7uq732da', '::1', 1627145465, _binary 0x5f5f63695f6c6173745f726567656e65726174657c693a313632373134353436353b637372666b65797c733a383a226b4531597033747a223b6373726676616c75657c733a32303a22364b454679585a74686c657275636b576734626e223b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b70617373776f72647c733a36303a22243261243037245365426b6e6e74705a726f723975796674566f706d7536317167306d733851763179563646472e6b514f534d2e3951686d546f3336223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c4e3b6c6173745f636865636b7c693a313632373134303634393b),
	('5ef29kracaggl3aisj3u37hk7j7tl5ku', '::1', 1627145789, _binary 0x5f5f63695f6c6173745f726567656e65726174657c693a313632373134353738393b637372666b65797c733a383a226b4531597033747a223b6373726676616c75657c733a32303a22364b454679585a74686c657275636b576734626e223b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b70617373776f72647c733a36303a22243261243037245365426b6e6e74705a726f723975796674566f706d7536317167306d733851763179563646472e6b514f534d2e3951686d546f3336223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c4e3b6c6173745f636865636b7c693a313632373134303634393b),
	('jqea7ed21hljnhmedqs6hoapa1t7h396', '::1', 1627146237, _binary 0x5f5f63695f6c6173745f726567656e65726174657c693a313632373134363233373b637372666b65797c733a383a226b4531597033747a223b6373726676616c75657c733a32303a22364b454679585a74686c657275636b576734626e223b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b70617373776f72647c733a36303a22243261243037245365426b6e6e74705a726f723975796674566f706d7536317167306d733851763179563646472e6b514f534d2e3951686d546f3336223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c4e3b6c6173745f636865636b7c693a313632373134303634393b),
	('2bda5pujtprlth3klbe2nh2bbbv5krn0', '::1', 1627146546, _binary 0x5f5f63695f6c6173745f726567656e65726174657c693a313632373134363534363b637372666b65797c733a383a226b4531597033747a223b6373726676616c75657c733a32303a22364b454679585a74686c657275636b576734626e223b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b70617373776f72647c733a36303a22243261243037245365426b6e6e74705a726f723975796674566f706d7536317167306d733851763179563646472e6b514f534d2e3951686d546f3336223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c4e3b6c6173745f636865636b7c693a313632373134303634393b),
	('b7fvc6tn0une9r0l4bb6ifqteb0jt60u', '::1', 1627147204, _binary 0x5f5f63695f6c6173745f726567656e65726174657c693a313632373134373230343b637372666b65797c733a383a226b4531597033747a223b6373726676616c75657c733a32303a22364b454679585a74686c657275636b576734626e223b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b70617373776f72647c733a36303a22243261243037245365426b6e6e74705a726f723975796674566f706d7536317167306d733851763179563646472e6b514f534d2e3951686d546f3336223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c4e3b6c6173745f636865636b7c693a313632373134303634393b),
	('2ijk68klnmldfpgenn19d5t1fklu8j9i', '::1', 1627147803, _binary 0x5f5f63695f6c6173745f726567656e65726174657c693a313632373134373830333b637372666b65797c733a383a226b4531597033747a223b6373726676616c75657c733a32303a22364b454679585a74686c657275636b576734626e223b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b70617373776f72647c733a36303a22243261243037245365426b6e6e74705a726f723975796674566f706d7536317167306d733851763179563646472e6b514f534d2e3951686d546f3336223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c4e3b6c6173745f636865636b7c693a313632373134303634393b),
	('b77e14ouj1tb5dea63dbjn4ju9rc0d6j', '::1', 1627148153, _binary 0x5f5f63695f6c6173745f726567656e65726174657c693a313632373134383135333b637372666b65797c733a383a226b4531597033747a223b6373726676616c75657c733a32303a22364b454679585a74686c657275636b576734626e223b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b70617373776f72647c733a36303a22243261243037245365426b6e6e74705a726f723975796674566f706d7536317167306d733851763179563646472e6b514f534d2e3951686d546f3336223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c4e3b6c6173745f636865636b7c693a313632373134303634393b),
	('rtroqr0l9cbc32ltp9rmqgta9rv00j5t', '::1', 1627148791, _binary 0x5f5f63695f6c6173745f726567656e65726174657c693a313632373134383739313b637372666b65797c733a383a226b4531597033747a223b6373726676616c75657c733a32303a22364b454679585a74686c657275636b576734626e223b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b70617373776f72647c733a36303a22243261243037245365426b6e6e74705a726f723975796674566f706d7536317167306d733851763179563646472e6b514f534d2e3951686d546f3336223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c4e3b6c6173745f636865636b7c693a313632373134303634393b),
	('913fjn3tvt1gjburgijq8s91s9b35fjv', '::1', 1627149103, _binary 0x5f5f63695f6c6173745f726567656e65726174657c693a313632373134393130333b637372666b65797c733a383a226b4531597033747a223b6373726676616c75657c733a32303a22364b454679585a74686c657275636b576734626e223b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b70617373776f72647c733a36303a22243261243037245365426b6e6e74705a726f723975796674566f706d7536317167306d733851763179563646472e6b514f534d2e3951686d546f3336223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c4e3b6c6173745f636865636b7c693a313632373134303634393b),
	('7b9lp6lfkpit6t4f5tr7976mp90gh4kd', '::1', 1627149447, _binary 0x5f5f63695f6c6173745f726567656e65726174657c693a313632373134393434373b637372666b65797c733a383a226b4531597033747a223b6373726676616c75657c733a32303a22364b454679585a74686c657275636b576734626e223b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b70617373776f72647c733a36303a22243261243037245365426b6e6e74705a726f723975796674566f706d7536317167306d733851763179563646472e6b514f534d2e3951686d546f3336223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c4e3b6c6173745f636865636b7c693a313632373134303634393b),
	('fhdv9l2hq9g9b3hhg98oojbuvefh6qq2', '::1', 1627151324, _binary 0x5f5f63695f6c6173745f726567656e65726174657c693a313632373135313332343b637372666b65797c733a383a226b4531597033747a223b6373726676616c75657c733a32303a22364b454679585a74686c657275636b576734626e223b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b70617373776f72647c733a36303a22243261243037245365426b6e6e74705a726f723975796674566f706d7536317167306d733851763179563646472e6b514f534d2e3951686d546f3336223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c4e3b6c6173745f636865636b7c693a313632373134303634393b),
	('tqjaacc0recd47l6e6dqrk8g2h7qt7ha', '::1', 1627151626, _binary 0x5f5f63695f6c6173745f726567656e65726174657c693a313632373135313632363b637372666b65797c733a383a226b4531597033747a223b6373726676616c75657c733a32303a22364b454679585a74686c657275636b576734626e223b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b70617373776f72647c733a36303a22243261243037245365426b6e6e74705a726f723975796674566f706d7536317167306d733851763179563646472e6b514f534d2e3951686d546f3336223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c4e3b6c6173745f636865636b7c693a313632373134303634393b),
	('7ea5rfr92cqj4p8ra3ca8p3nosuts7s5', '::1', 1627152430, _binary 0x5f5f63695f6c6173745f726567656e65726174657c693a313632373135323433303b637372666b65797c733a383a226b4531597033747a223b6373726676616c75657c733a32303a22364b454679585a74686c657275636b576734626e223b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b70617373776f72647c733a36303a22243261243037245365426b6e6e74705a726f723975796674566f706d7536317167306d733851763179563646472e6b514f534d2e3951686d546f3336223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c4e3b6c6173745f636865636b7c693a313632373134303634393b),
	('b9r3rnjogps8sneqsap5u6avifh4fchi', '::1', 1627152430, _binary 0x5f5f63695f6c6173745f726567656e65726174657c693a313632373135323433303b637372666b65797c733a383a226b4531597033747a223b6373726676616c75657c733a32303a22364b454679585a74686c657275636b576734626e223b6964656e746974797c733a31353a2261646d696e4061646d696e2e636f6d223b656d61696c7c733a31353a2261646d696e4061646d696e2e636f6d223b70617373776f72647c733a36303a22243261243037245365426b6e6e74705a726f723975796674566f706d7536317167306d733851763179563646472e6b514f534d2e3951686d546f3336223b757365725f69647c733a313a2231223b6f6c645f6c6173745f6c6f67696e7c4e3b6c6173745f636865636b7c693a313632373134303634393b);
/*!40000 ALTER TABLE `ci_sessions` ENABLE KEYS */;

-- Volcando estructura para tabla pixomaticesf.companies
CREATE TABLE IF NOT EXISTS `companies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) NOT NULL DEFAULT '0',
  `name` varchar(100) DEFAULT NULL,
  `cif` varchar(9) DEFAULT NULL,
  `shortdesc` varchar(100) DEFAULT NULL,
  `description` longtext,
  `email` varchar(50) DEFAULT NULL,
  `ccc` varchar(100) DEFAULT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `logo` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=55 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla pixomaticesf.companies: 46 rows
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
INSERT INTO `companies` (`id`, `id_user`, `name`, `cif`, `shortdesc`, `description`, `email`, `ccc`, `date`, `status`, `logo`) VALUES
	(1, 3165, 'EMP gato 1', '1aaaaaaa', 'Descripcion breve', 'descripcion larga muy larga descripcion larga muy larga', 'email@email.com', 'es1111111111111111111111', '2021-07-22 18:39:16', 0, 'https://static.eldiario.es/clip/c1e03bad-0d3e-4324-b803-7b60fadbd83b_16-9-aspect-ratio_default_0.jpg'),
	(25, 3165, 'EMP gato 20', '1aaaaaaav', 'Descripcion breve', 'DADADAS', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-24 22:46:58', 1, 'https://cdn.pixabay.com/photo/2016/07/10/21/47/cat-1508613_960_720.jpg'),
	(23, 3165, 'EMP gato 18', '1aaaaaaat', 'Descripcion breve', 'DADADAS', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-24 22:46:58', 1, 'https://cdn.pixabay.com/photo/2014/11/30/14/11/cat-551554_960_720.jpg'),
	(5, 3165, 'EMP gato 3', '1aaaaaaac', 'Descripcion breve', 'describiendo empresas para la prueba técnica', 'a.ramossan@gmail.com', 'es1111111111111111111111', '2021-07-26 18:00:51', 1, 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gatito-naranja-mesa.jpg'),
	(6, 3165, 'EMP gato 4', '1aaaaaaad', 'Descripcion breve', 'descripcion larga muy larga descripcion larga muy larga', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 18:48:25', 0, 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-caja_0.jpg'),
	(7, 3165, 'EMP gato 5', '1aaaaaaae', 'Descripcion breve', 'descripcion añadida desde gestor gatitos bonitos', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 18:48:52', 1, 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-persa-bandeja.jpg'),
	(8, 3165, 'EMP gato 6', '1aaaaaaaf', 'Descripcion breve', 'describiendo empresas para la prueba técnica', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 18:54:23', 1, 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-frotandose-piernas.jpg'),
	(9, 3165, 'EMP gato 5', '1aaaaaaag', 'Descripcion breve', 'descripcion larga muy larga descripcion larga muy larga', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:07:57', 0, 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-amasando.jpg'),
	(10, 3165, 'EMP gato 6', '1aaaaaaah', 'Descripcion breve', 'describiendo empresas para la prueba técnica', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:09:47', 0, 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-noche.jpg'),
	(11, 3165, 'EMP gato 7', '1aaaaaaai', 'Descripcion breve', 'describiendo empresas para la prueba técnica', 'a.ramossan@gmail.com', 'es1111111111111111111111', '2021-07-26 19:14:08', 0, 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-boca-abierta.jpg'),
	(12, 3165, 'EMP gato 8', '1aaaaaaaj', 'Descripcion breve', 'descripcion añadida desde gestor gatitos bonitos', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://cdn.pixabay.com/photo/2016/07/10/21/47/cat-1508613_960_720.jpg'),
	(24, 3165, 'EMP gato 19', '1aaaaaaau', 'Descripcion breve', 'DADADAS', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-24 22:46:58', 1, 'https://cdn.pixabay.com/photo/2014/04/13/20/49/cat-323262_960_720.jpg'),
	(13, 3165, 'EMP gato 9', '1aaaaaaak', 'Descripcion breve', 'descripcion larga muy larga descripcion larga muy larga', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gatos-grises-comiendo.jpg'),
	(14, 3165, 'EMP gato 10', '1aaaaaaal', 'Descripcion breve', 'descripcion añadida desde gestor gatitos bonitos', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-mirando-ventana.jpg'),
	(15, 3165, 'EMP gato 11', '1aaaaaaam', 'Descripcion breve', 'descripcion añadida desde gestor gatitos bonitos', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-dormido-boca-arriba.jpg'),
	(16, 3165, 'EMP gato 12', '1aaaaaaan', 'Descripcion breve', 'describiendo empresas para la prueba técnica', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-blanco-negro-rascador.jpg'),
	(17, 3165, 'EMP gato 13', '1aaaaaaao', 'Descripcion breve', 'descripcion larga muy larga descripcion larga muy larga', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-naranja-dormido.jpg'),
	(18, 3165, 'EMP gato 14', '1aaaaaaap', 'Descripcion breve', 'describiendo empresas para la prueba técnica', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-hombre.jpg'),
	(19, 3165, 'EMP gato 15', '1aaaaaaaq', 'Descripcion breve', 'describiendo empresas para la prueba técnica', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-blanco-mujer.jpg'),
	(20, 3165, 'EMP gato 16', '1aaaaaaar', 'Descripcion breve', 'descripcion larga muy larga descripcion larga muy larga', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://www.fundacion-affinity.org/sites/default/files/el-gato-necesita-tener-acceso-al-exterior.jpg'),
	(26, 3165, 'EMP gato 21', '1aaaaaaaw', 'Descripcion breve', 'describiendo empresas para la prueba técnica', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-24 22:46:58', 0, 'https://cdn.pixabay.com/photo/2017/11/09/21/41/cat-2934720_960_720.jpg'),
	(22, 3165, 'EMP gato 17', '1aaaaaaas', 'Descripcion breve', 'describiendo empresas para la prueba técnica', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://n.com.do/wp-content/uploads/2021/06/gatos-gestos-m.jpg'),
	(4, 3165, 'EMP gato 2', '1aaaaaab', 'Descripcion breve', 'descripcion añadida desde gestor gatitos bonitos', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-24 22:45:48', 1, 'https://estaticos.muyinteresante.es/media/cache/1140x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-marron_0.jpg'),
	(27, 3165, 'EMP gato 22', '1aaaaaaax', 'Descripcion breve', 'DADADAS', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-24 22:46:58', 1, 'https://ichef.bbci.co.uk/news/640/cpsprodpb/8536/production/_103520143_gettyimages-908714708.jpg'),
	(28, 3165, 'EMP gato 23', '1aaaaaaay', 'Descripcion breve', 'descripcion añadida desde gestor gatitos bonitos', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-pajaro-muerto.jpg'),
	(29, 3165, 'EMP gato 24', '1aaaaaaaz', 'Descripcion breve', 'descripcion larga muy larga descripcion larga muy larga', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-pajaro-muerto.jpg'),
	(30, 3165, 'EMP gato 25', '1aaaaaabb', 'Descripcion breve', 'describiendo empresas para la prueba técnica', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-pajaro-muerto.jpg'),
	(31, 3165, 'EMP gato 26', '1aaaaaabc', 'Descripcion breve', 'describiendo empresas para la prueba técnica', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-pajaro-muerto.jpg'),
	(32, 3165, 'EMP gato 27', '1aaaaaabd', 'Descripcion breve', 'describiendo empresas para la prueba técnica', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://cdn.pixabay.com/photo/2016/07/10/21/47/cat-1508613_960_720.jpg'),
	(33, 3165, 'EMP gato 28', '1aaaaaabe', 'Descripcion breve', 'descripcion larga muy larga descripcion larga muy larga', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-pajaro-muerto.jpg'),
	(34, 3165, 'EMP gato 29', '1aaaaaabf', 'Descripcion breve', 'describiendo empresas para la prueba técnica', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-pajaro-muerto.jpg'),
	(35, 3165, 'EMP gato 30', '1aaaaaabg', 'Descripcion breve', 'descripcion añadida desde gestor gatitos bonitos', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-pajaro-muerto.jpg'),
	(36, 3165, 'EMP gato 31', '1aaaaaabh', 'Descripcion breve', 'describiendo empresas para la prueba técnica', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-pajaro-muerto.jpg'),
	(37, 3165, 'EMP gato 32', '1aaaaaabi', 'Descripcion breve', 'describiendo empresas para la prueba técnica', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://static.eldiario.es/clip/c1e03bad-0d3e-4324-b803-7b60fadbd83b_16-9-aspect-ratio_default_0.jpg'),
	(38, 3165, 'EMP gato 33', '1aaaaaabj', 'Descripcion breve', 'describiendo empresas para la prueba técnica', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-pajaro-muerto.jpg'),
	(39, 3165, 'EMP gato 34', '1aaaaaabk', 'Descripcion breve', 'descripcion larga muy larga descripcion larga muy larga', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-pajaro-muerto.jpg'),
	(40, 3165, 'EMP gato 35', '1aaaaaabl', 'Descripcion breve', 'describiendo empresas para la prueba técnica', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-pajaro-muerto.jpg'),
	(41, 3165, 'EMP gato 36', '1aaaaaabm', 'Descripcion breve', 'describiendo empresas para la prueba técnica', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-pajaro-muerto.jpg'),
	(42, 3165, 'EMP gato 37', '1aaaaaabn', 'Descripcion breve', 'describiendo empresas para la prueba técnica', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-pajaro-muerto.jpg'),
	(43, 3165, 'EMP gato 38', '1aaaaaabo', 'Descripcion breve', 'describiendo empresas para la prueba técnica', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-pajaro-muerto.jpg'),
	(44, 3165, 'EMP gato 39', '1aaaaaabp', 'Descripcion breve', 'describiendo empresas para la prueba técnica', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://www.fundacion-affinity.org/sites/default/files/el-gato-necesita-tener-acceso-al-exterior.jpg'),
	(45, 3165, 'EMP gato 40', '1aaaaaabq', 'Descripcion breve', 'descripcion larga muy larga descripcion larga muy larga', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://estaticos.muyinteresante.es/media/cache/1000x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-pajaro-muerto.jpg'),
	(46, 3165, 'EMP gato 41', '1aaaaaabr', 'Descripcion breve', 'describiendo empresas para la prueba técnica', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://cdn.pixabay.com/photo/2014/11/30/14/11/cat-551554_960_720.jpg'),
	(47, 3165, 'EMP gato 42', '1aaaaaabs', 'Descripcion breve', 'describiendo empresas para la prueba técnica', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://cdn.pixabay.com/photo/2014/04/13/20/49/cat-323262_960_720.jpg'),
	(48, 3165, 'EMP gato 43', '1aaaaaabt', 'Descripcion breve', 'describiendo empresas para la prueba técnica', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://ichef.bbci.co.uk/news/640/cpsprodpb/8536/production/_103520143_gettyimages-908714708.jpg'),
	(49, 3165, 'EMP gato 44', '1aaaaaabu', 'Descripcion breve', 'describiendo empresas para la prueba técnica', 'edukenobi@hotmail.es', 'es1111111111111111111111', '2021-07-26 19:18:18', 0, 'https://estaticos.muyinteresante.es/media/cache/1140x_thumb/uploads/images/gallery/59c4f5655bafe82c692a7052/gato-marron_0.jpg');
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;

-- Volcando estructura para tabla pixomaticesf.companies_bad
CREATE TABLE IF NOT EXISTS `companies_bad` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 ROW_FORMAT=DYNAMIC;

-- Volcando datos para la tabla pixomaticesf.companies_bad: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `companies_bad` DISABLE KEYS */;
INSERT INTO `companies_bad` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
	(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1627140649, 1, 'Admin', 'Istrator', 'ADMIN', '0'),
	(2, '127.0.0.1', 'manager', '$2y$08$uHgImo7BJNoIIb3GgO2PqOGNnj37TUzTej30qOGiqqZIi5wX8iNlS', '', 'manager@manager.com', '', NULL, NULL, NULL, 1268889823, NULL, 1, 'Mana', 'Ger', 'MANAGER', '0');
/*!40000 ALTER TABLE `companies_bad` ENABLE KEYS */;

-- Volcando estructura para tabla pixomaticesf.debug
CREATE TABLE IF NOT EXISTS `debug` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `fecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `data` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla pixomaticesf.debug: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `debug` DISABLE KEYS */;
/*!40000 ALTER TABLE `debug` ENABLE KEYS */;

-- Volcando estructura para tabla pixomaticesf.favorites
CREATE TABLE IF NOT EXISTS `favorites` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_company` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM AUTO_INCREMENT=65 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla pixomaticesf.favorites: 7 rows
/*!40000 ALTER TABLE `favorites` DISABLE KEYS */;
INSERT INTO `favorites` (`id`, `id_user`, `id_company`) VALUES
	(4, 3165, 4),
	(5, 3165, 5),
	(6, 3165, 6),
	(7, 3165, 7),
	(8, 3165, 8),
	(9, 3165, 9),
	(64, 3165, 23);
/*!40000 ALTER TABLE `favorites` ENABLE KEYS */;

-- Volcando estructura para tabla pixomaticesf.favorites_copy
CREATE TABLE IF NOT EXISTS `favorites_copy` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_user` int(11) DEFAULT NULL,
  `id_company` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=MyISAM DEFAULT CHARSET=utf8 ROW_FORMAT=FIXED;

-- Volcando datos para la tabla pixomaticesf.favorites_copy: 0 rows
/*!40000 ALTER TABLE `favorites_copy` DISABLE KEYS */;
/*!40000 ALTER TABLE `favorites_copy` ENABLE KEYS */;

-- Volcando estructura para tabla pixomaticesf.groups
CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL,
  `bgcolor` char(7) NOT NULL DEFAULT '#607D8B',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla pixomaticesf.groups: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `groups` DISABLE KEYS */;
INSERT INTO `groups` (`id`, `name`, `description`, `bgcolor`) VALUES
	(1, 'admin', 'Superadministrator', '#F44336'),
	(2, 'managers', 'Managers', '#009688'),
	(3, 'members', 'General User', '#2196F3');
/*!40000 ALTER TABLE `groups` ENABLE KEYS */;

-- Volcando estructura para tabla pixomaticesf.login_attempts
CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla pixomaticesf.login_attempts: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `login_attempts` DISABLE KEYS */;
/*!40000 ALTER TABLE `login_attempts` ENABLE KEYS */;

-- Volcando estructura para tabla pixomaticesf.public_preferences
CREATE TABLE IF NOT EXISTS `public_preferences` (
  `id` int(1) NOT NULL AUTO_INCREMENT,
  `transition_page` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla pixomaticesf.public_preferences: ~0 rows (aproximadamente)
/*!40000 ALTER TABLE `public_preferences` DISABLE KEYS */;
INSERT INTO `public_preferences` (`id`, `transition_page`) VALUES
	(1, 0);
/*!40000 ALTER TABLE `public_preferences` ENABLE KEYS */;

-- Volcando estructura para tabla pixomaticesf.users
CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ip_address` varchar(15) NOT NULL,
  `username` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla pixomaticesf.users: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
	(1, '127.0.0.1', 'administrator', '$2a$07$SeBknntpZror9uyftVopmu61qg0ms8Qv1yV6FG.kQOSM.9QhmTo36', '', 'admin@admin.com', '', NULL, NULL, NULL, 1268889823, 1627140649, 1, 'Admin', 'Istrator', 'ADMIN', '0'),
	(2, '127.0.0.1', 'manager', '$2y$08$uHgImo7BJNoIIb3GgO2PqOGNnj37TUzTej30qOGiqqZIi5wX8iNlS', '', 'manager@manager.com', '', NULL, NULL, NULL, 1268889823, NULL, 1, 'Mana', 'Ger', 'MANAGER', '0');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;

-- Volcando estructura para tabla pixomaticesf.users_groups
CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`),
  KEY `fk_users_groups_users1_idx` (`user_id`),
  KEY `fk_users_groups_groups1_idx` (`group_id`),
  CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- Volcando datos para la tabla pixomaticesf.users_groups: ~2 rows (aproximadamente)
/*!40000 ALTER TABLE `users_groups` DISABLE KEYS */;
INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
	(1, 1, 1),
	(2, 2, 2);
/*!40000 ALTER TABLE `users_groups` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
