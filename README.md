# What is this?

> 
> Prueba técnica para Bluumi con Codeigniter 3 - Eduardo Sánchez Ferrer
> 

## What is inside?

1. Login con email de la api de usuarios
2. Listar/Crear/Editar empresas gatunas
3. Guardar empresas gatunas favoritas
4. Listar usuarios de la api de usuarios
5. Visualizar datos de los usuarios
6. Visualizar posts de los usuarios


## Setup

1. **Base name**

    base_url() se configura en application/config/config.php
   
2. **Database**

    Se configura en application/config/database.php.
    el archivo sql para cargar la bbdd se encuentra en pixomatic/install
   
